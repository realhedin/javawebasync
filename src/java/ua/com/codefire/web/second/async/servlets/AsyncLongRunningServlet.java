/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.web.second.async.servlets;

import java.io.IOException;

import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.com.codefire.web.second.async.listeners.AppAsyncListener;
import ua.com.codefire.web.second.async.tasks.AsyncRequestProcessor;

/**
 *
 * @author CodeFire
 */
@WebServlet(urlPatterns = "/async-long-running", asyncSupported = true)
public class AsyncLongRunningServlet extends HttpServlet {

    private static final int asyncTimeout = 10000;

    private static final Logger LOGGER = Logger.getLogger(AsyncLongRunningServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long startTime = System.currentTimeMillis();
        LOGGER.log(Level.INFO, "Start {0}.", Thread.currentThread().getName());

        AsyncContext asyncContext = request.startAsync();
        asyncContext.addListener(new AppAsyncListener());
        asyncContext.setTimeout(asyncTimeout);

        ExecutorService executor = (ExecutorService) request.getServletContext().getAttribute("executor");

        executor.submit(new AsyncRequestProcessor(asyncContext));
        long endTime = System.currentTimeMillis();
        LOGGER.log(Level.INFO, "End {0}. Elapsed: {1} ms.", new Object[]{Thread.currentThread().getName(), endTime - startTime});
    }

}
