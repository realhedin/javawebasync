/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.web.second.async.tasks;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.AsyncContext;
import javax.servlet.ServletResponse;

/**
 *
 * @author CodeFire
 */
public class AsyncRequestProcessor implements Runnable {

    // TODO: set to 'true' for emulate long time operations
    private static final int actionsLimit = 10;
    private static final int ms = 500;

    private AsyncContext asyncContext;

    public AsyncRequestProcessor() {
    }

    public AsyncRequestProcessor(AsyncContext asyncContext) {
        this.asyncContext = asyncContext;
    }

    @Override
    public void run() {
        try {
            ServletResponse asyncResponse = asyncContext.getResponse();
            asyncResponse.setContentType("text/html");
            PrintWriter out = asyncResponse.getWriter();
            out.write("Processing begin!<br />");
            for (int action = 0; action < actionsLimit; action++) {
                longProcessing(ms);
                // using async response
                out.printf("Action #%d completed by %s.<br />", action, Thread.currentThread().getName());
                out.flush();
            }
            out.write("Processing complete!");
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(AsyncRequestProcessor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //complete the processing
            asyncContext.complete();
        }
    }

    private void longProcessing(int secs) {
        try {
            Thread.sleep(secs);
        } catch (InterruptedException ex) {
            Logger.getLogger(AsyncRequestProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
